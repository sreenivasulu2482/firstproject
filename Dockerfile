# Docker file for downloading and Installing Visual Studio 2017
# Build command: docker build -t sreenivas/vs2017:1.0 . --network "Default Switch"

FROM mcr.microsoft.com/windows/servercore:10.0.17763.1282

SHELL ["powershell.exe", "-ExecutionPolicy", "Bypass", "-Command"]

RUN Invoke-WebRequest -Uri 'https://download.visualstudio.microsoft.com/download/pr/014120d7-d689-4305-befd-3cb711108212/1f81f3962f75eff5d83a60abd3a3ec7b/ndp48-web.exe' -OutFile ./ndp48-web.exe

RUN Start-Process .\ndp48-web.exe -ArgumentList '/quiet','/norestart' -Wait
		  

